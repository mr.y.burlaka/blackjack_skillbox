// Fill out your copyright notice in the Description page of Project Settings.


#include "WidgetGame.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "BlackJack/BlackJackGameModeBase.h"

void UWidgetGame::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (GetWorld())
	{
		BJGameMode = GetWorld()->GetAuthGameMode<ABlackJackGameModeBase>();
		if (BJGameMode)
		{
			BJGameMode->OnRoundStart.AddUObject(this, &UWidgetGame::OnStartRound);
			BJGameMode->OnRoundEnd.AddUObject(this, &UWidgetGame::OnEndRound);
		}
	}

	if (StartButton)
	{
		StartButton->OnClicked.AddDynamic(this, &UWidgetGame::OnStartButtonPressed);
	}
	if (HitButton)
	{
		HitButton->OnClicked.AddDynamic(this, &UWidgetGame::OnHitButtonPressed);
	}
	if (StandButton)
	{
		StandButton->OnClicked.AddDynamic(this, &UWidgetGame::OnStandButtonPressed);
	}
}

void UWidgetGame::OnStartButtonPressed()
{
	if (!BJGameMode) return;
	BJGameMode->StartRound();
}

void UWidgetGame::OnHitButtonPressed()
{
	if (!BJGameMode) return;
	BJGameMode->PlayerHit();
}

void UWidgetGame::OnStandButtonPressed()
{
	if (!BJGameMode) return;
	BJGameMode->PlayerStand();
}

void UWidgetGame::OnStartRound()
{
	SetElementVisibility(true);
}

void UWidgetGame::OnEndRound(EEndState EndState)
{
	checkf(EndTexts.Contains(EndState) && EndTextsColor.Contains(EndState), TEXT("No Text or Color for EndState. Check Widget!!!"));
	ResultText->SetText(EndTexts[EndState]);
	ResultText->SetColorAndOpacity(EndTextsColor[EndState]);
	SetElementVisibility(false);
}

void UWidgetGame::SetElementVisibility(bool bIsStart) const
{
	ResultText->SetVisibility(bIsStart ? ESlateVisibility::Collapsed : ESlateVisibility::Visible);
	StartButton->SetVisibility(bIsStart ? ESlateVisibility::Collapsed : ESlateVisibility::Visible);
	HitButton->SetVisibility(bIsStart ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
	StandButton->SetVisibility(bIsStart ? ESlateVisibility::Visible : ESlateVisibility::Collapsed);
}

int32 UWidgetGame::GetPlayerTotal() const
{
	if (!BJGameMode) return 0;
	return BJGameMode->GetPlayerTotal();
}

int32 UWidgetGame::GetDealerTotal() const
{
	if (!BJGameMode) return 0;
	return BJGameMode->GetDealerTotal();
}