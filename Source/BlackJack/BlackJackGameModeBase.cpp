// Copyright Epic Games, Inc. All Rights Reserved.


#include "BlackJackGameModeBase.h"
#include "Card.h"
#include "Deck.h"
#include "BJUtils.h"
#include "Kismet/GameplayStatics.h"

void ABlackJackGameModeBase::StartPlay()
{
	Super::StartPlay();

	/** Find ADeck in World and save its pointer to variable */
	TArray<AActor*> TempActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ADeck::StaticClass(), TempActors);
	checkf(TempActors.Num() > 0, TEXT("Missing Deck on level!!!"));
	MainDeck = Cast<ADeck>(TempActors[0]);
}

/** Set all default values and deal one card for player and one card for dealer */
void ABlackJackGameModeBase::StartRound()
{
	if (!MainDeck) return;
	ClearDecks();
	NextPlayerCardPosition = StartPlayerCardsPosition;
	NextDealerCardPosition = StartDealerCardsPosition;
	PlayerTotal = 0;
	DealerTotal = 0;
	MainDeck->CreateShuffledDeck();

	PlayerHit();
	DealerHit();

	OnRoundStart.Broadcast();
}

void ABlackJackGameModeBase::EndRound(const EEndState State)
{
	GetWorldTimerManager().ClearTimer(DealerHitTimerHande);
	OnRoundEnd.Broadcast(State);
}

/** Deal card to player and check player's total for win or lose */
void ABlackJackGameModeBase::PlayerHit()
{
	if (!MainDeck) return;
	PlayerTakeCard(MainDeck->GetCardOnTop());
	if (PlayerTotal == 21)
	{
		EndRound(EEndState::EES_PlayerWins);
	}
	else if (PlayerTotal > 21)
	{
		EndRound(EEndState::EES_DealerWins);
	}
}

/** When player stand StartTimer for dealer hits */
void ABlackJackGameModeBase::PlayerStand()
{
	GetWorldTimerManager().SetTimer(DealerHitTimerHande, this, &ABlackJackGameModeBase::DealerHit, 1.f, true, 0.1f);
}

/** Deal card to dealer and check dealer's total for win or lose */
void ABlackJackGameModeBase::DealerHit()
{
	if (!MainDeck) return;
	DealerTakeCard(MainDeck->GetCardOnTop());
	if (DealerTotal == 21)
	{
		EndRound(EEndState::EES_DealerWins);
	}
	else if (DealerTotal > 21)
	{
		EndRound(EEndState::EES_PlayerWins);
	}
	else if (DealerTotal > 16)
	{
		if (DealerTotal == PlayerTotal)
		{
			EndRound(EEndState::EES_Tie);
		}
		else if (DealerTotal > PlayerTotal)
		{
			EndRound(EEndState::EES_DealerWins);
		}
		else
		{
			EndRound(EEndState::EES_PlayerWins);
		}
	}
}


int32 ABlackJackGameModeBase::CalculateDeck(TArray<ACard*>& Deck)
{
	if (Deck.Num() <= 0) return 0;

	int32 Total = 0;
	int32 AceCount = 0;

	for (const auto Card : Deck)
	{
		if (!Card) continue;
		Total += Card->GetCardValue();
		if (Card->GetRank() == ERank::ERT_Ace)
		{
			AceCount++;
		}
	}
	if (AceCount <= 0) return Total;

	while (AceCount > 0)
	{
		if (Total > 21)
		{
			Total -= 10;
			if (Total <= 21) break;
		}
		AceCount--;
	}
	return Total;
}

void ABlackJackGameModeBase::ClearDecks()
{
	Utils::ClearDeck(PlayerCards);
	Utils::ClearDeck(DealerCards);
}

/** Player take new card and calculate player's total without check */
void ABlackJackGameModeBase::PlayerTakeCard(ACard* Card)
{
	if (!Card) return;
	PlayerCards.Add(Card);
	PlayerTotal = CalculateDeck(PlayerCards);
	Card->MoveCard(NextPlayerCardPosition);
	NextPlayerCardPosition.Y += CardsYOffset;
	NextPlayerCardPosition.Z += ZOffsetBetweenCards;
}

/** Dealer take new card and calculate dealer's total without check */
void ABlackJackGameModeBase::DealerTakeCard(ACard* Card)
{
	if (!Card) return;
	DealerCards.Add(Card);
	DealerTotal = CalculateDeck(DealerCards);
	Card->MoveCard(NextDealerCardPosition);
	NextDealerCardPosition.Y += CardsYOffset;
	NextDealerCardPosition.Z += ZOffsetBetweenCards;
}
